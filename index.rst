.. index::
   ! Build automation
   ! Command runners

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-build/rss.xml>`_

.. _build_tuto:

==========================================
**Build automation and command runners**
==========================================


- https://en.wikipedia.org/wiki/Build_automation
- https://en.wikipedia.org/wiki/Category:Build_automation
- https://en.wikipedia.org/wiki/List_of_build_automation_software
- https://github.com/coreinfrastructure/best-practices-badge/blob/master/doc/criteria.md#build


.. figure:: images/Buildroot_logo.png
   :align: center


Build automation is the process of automating the creation of a software build
and the associated processes including: compiling computer source code into
binary code, packaging binary code, and running automated tests.


.. figure:: _static/building_tools.png
   :align: center


Best practices
===============

.. toctree::
   :maxdepth: 3

   best_practices/best_practices


News
=======

.. toctree::
   :maxdepth: 3

   news/news


Bundlers
================

.. toctree::
   :maxdepth: 3

   bundlers/bundlers


Runtime managers
================

.. toctree::
   :maxdepth: 3

   asdf/asdf
   mise/mise
   rtx/rtx


Declarative CLI Version Manager
===================================

.. toctree::
   :maxdepth: 3

   aqua/aqua



Building tools
================

.. toctree::
   :maxdepth: 3

   cmake/cmake
   gnu_make/gnu_make
   gradle/gradle
   gyp/gyp
   just/just
   maven/maven
   nix/nix
   pyinvoke/pyinvoke
   waf/waf
