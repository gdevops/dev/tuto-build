.. index::
   pair: nix; Versions

.. _nix_versions:

===============================================
Nix versions
===============================================

.. seealso::

   - https://github.com/NixOS/nix/releases

    
.. toctree::
   :maxdepth: 3

   2.3.4/2.3.4
