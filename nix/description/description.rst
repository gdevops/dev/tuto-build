
.. _nix_description:

===============================================
Nix description
===============================================

.. seealso::

   - https://github.com/NixOS/nix
   - https://nixos.org/nix/
   - https://github.com/NixOS/nixpkgs
   - https://github.com/NixOS/nixos-weekly
   - https://github.com/NixOS/nixos-weekly/pulls
   
   
.. contents::
   :depth: 3
   
Description
===============

Nix is a purely functional package manager. 

This means that it treats packages like values in purely functional 
programming languages such as Haskell — they are built by functions 
that don’t have side-effects, and they never change after they have 
been built. 

Nix stores packages in the Nix store, usually the directory /nix/store, 
where each package has its own unique subdirectory such as

::

    /nix/store/b6gvzjyb2pg0kjfwrjmg1vfhh54ad73z-firefox-33.1/

where b6gvzjyb2pg0... is a unique identifier for the package that 
captures all its dependencies (it’s a cryptographic hash of the package’s 
build dependency graph). 

This enables many powerful features.


Multiple versions
=================

You can have multiple versions or variants of a package installed at 
the same time. 
This is especially important when different applications have dependencies 
on different versions of the same package — it prevents the **DLL hell**

Because of the hashing scheme, different versions of a package end up in 
different paths in the Nix store, so they don’t interfere with each other.

**An important consequence is that operations like upgrading or uninstalling 
an application cannot break other applications**, since these operations 
never **destructively** update or delete files that are used by other packages.

