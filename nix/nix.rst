.. index::
   pair: nix; the purely functional package manager
   ! Nix

.. _nix:

===============================================
Nix
===============================================

.. seealso::

   - https://github.com/NixOS/nix
   - https://nixos.org/nix/
   - https://github.com/NixOS/nixpkgs
   - https://github.com/NixOS/nixos-weekly
   - https://github.com/NixOS/nixos-weekly/pulls


.. figure:: nixos-logo-only-hires.png
   :align: center

   https://nixos.org/nix/

.. toctree::
   :maxdepth: 3

   description/description
   articles/articles
   versions/versions
