

.. index::
   pair: GYP; Definition


.. _gyp_definition:

================
GYP definition
================

.. seealso::

   - https://en.wikipedia.org/wiki/GYP_(software)


.. contents::
   :depth: 3

Wikipedia definition
=====================

**GYP** (generate your projects) is a build automation tool.

GYP was created by Google to generate native IDE project files (such as Visual
Studio Code and Xcode) for building the Chromium web browser and is licensed
as open source software using the BSD software license.

The functionality of GYP is similar to the CMake build tool.

GYP processes a file that contains a JSON dictionary in order to generate one
or more target project make files.
The single source .GYP file is generic while the target files are specific to
each targeted build tool.

Software projects being built using GYP include the V8 Javascript engine,
Google's Chromium web browser, Dart, Node.js, WebRTC, and Telegram.

In 2016 the Chromium project replaced GYP with GN


GYP definition
================

.. seealso::

   - https://gyp.gsrc.io/

GYP is a Meta-Build system: a build system that generates other build systems.

GYP is intended to support large projects that need to be built on multiple
platforms (e.g., Mac, Windows, Linux), and where it is important that the
project can be built using the IDEs that are popular on each platform as if
the project is a “native” one.

It can be used to generate XCode projects, Visual Studio projects, Ninja build
files, and Makefiles.

In each case GYP's goal is to replicate as closely as possible the way one would
set up a native build of the project using the IDE.

GYP can also be used to generate “hybrid” projects that provide the IDE
scaffolding for a nice user experience but call out to Ninja to do the actual
building (which is usually much faster than the native build systems of the IDEs).
