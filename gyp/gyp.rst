
.. index::
   ! GYP
   ! Generate Your Projects

.. _gyp:

==============================
GYP
==============================

.. seealso::

   - https://chromium.googlesource.com/external/gyp
   - https://gyp.gsrc.io/
   - https://gyp.gsrc.io/docs/GypVsCMake.md


.. toctree::
   :maxdepth: 3

   definition/definition
