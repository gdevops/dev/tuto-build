
.. index::
   pair: Command runner; just
   ! Just

.. _just:

==================================================================
**Just** (a handy way to save and run project-specific commands)
==================================================================

- https://github.com/casey/just
- https://just.systems/man/en/
- https://just.systems/


Documentation
===============

- https://github.com/casey/just/blob/master/README.md

Examples
=========

- https://github.com/simonw/sqlite-utils/blob/c7cad6fc257c178b24b3f574b8c6992002c6f072/Justfile
- https://til.simonwillison.net/django/just-with-django


`Jeff Triplett <https://x.com/webology/status/1532860591307726851>`_ convinced me to take a look at just as a command automation
tool - sort of an alternative to Make, except with a focus on commands
rather than managing build dependencies.

I really like it, and I've started using it for my own Django projects.
