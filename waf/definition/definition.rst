
.. index::
   pair: waf ; definition

.. _waf_def:

================
waf definition
================

.. seealso::

   - https://en.wikipedia.org/wiki/Waf
   - https://gitlab.com/ita1024/waf/
   - https://waf.io/projects_using_waf.html

.. contents::
   :depth: 3

English wikipedia definition
==============================

Waf is a build automation tool designed to assist in the automatic compilation
and installation of computer software.

It is written in Python and maintained by Thomas Nagy.

Waf's source code is open source software, released under the terms of the
New BSD License, though its accompanying documentation is under the CC-BY-NC-ND
license, which forbids both modification and commercial redistribution:
this prevents vendors such as the Debian project from including Waf documentation
in their distributions.


History
=========

Thomas Nagy created a build automation tool called BKsys which was designed to
sit on top of SCons, providing higher-level functionality similar to that of
Autotools.

This was part of an effort for switching KDE away from Autotools to a more
modern build system in the beginning stages of the KDE 4 development cycle.

BKsys/SCons was chosen by the KDE community as their new standard build system.

When Thomas Nagy decided that SCons's fundamental issues (most notably the poor
scalability) were too complex and time-consuming to fix, he started a complete
rewrite which he named Waf. With BKsys being recognized as a dead end, KDE
decided to switch to CMake instead; however, Waf continued to be maintained
as an individual project and has since seen prolific development and adoption
by other communities.



.. include:: README.rst
