
.. index::
   ! waf

.. _waf:

================
waf
================

.. seealso::

   - https://en.wikipedia.org/wiki/Waf
   - https://gitlab.com/ita1024/waf/


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
