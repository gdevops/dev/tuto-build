
.. index::
   ! Maven

.. _maven:

==============================
Maven
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_Maven
   - https://maven.apache.org/

.. figure:: Maven_logo.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
