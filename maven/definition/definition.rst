
.. index::
   pair: Maven; Definition

.. _maven_definition:

==============================
Maven definition
==============================

.. contents::
   :depth: 3


Wikipedia definition
======================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_Maven

Maven is a build automation tool used primarily for Java projects.

Maven addresses two aspects of building software: first, it describes how
software is built,[clarification needed] and second, it describes its
dependencies.

Unlike earlier tools like Apache Ant, it uses conventions for the build
procedure, and only exceptions need to be written down. An XML file describes
the software project being built, its dependencies on other external modules
and components, the build order, directories, and required plug-ins.
It comes with pre-defined targets for performing certain well-defined tasks
such as compilation of code and its packaging.
