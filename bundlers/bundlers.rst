

.. index::
   ! Assets manager
   ! Bundlers


.. _assets_manager:
.. _bundlers:

============================
Bundlers
============================

.. seealso::

   - https://github.com/sorrycc/awesome-javascript#bundlers
   - https://js.libhunt.com/categories/3-bundlers


.. toctree::
   :maxdepth: 3

   gulp/gulp
   parcel/parcel
   rollup/rollup
   webpack/webpack
   for_django/for_django
