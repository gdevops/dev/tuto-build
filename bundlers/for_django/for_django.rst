

.. index::
   pair: Assets manager ; Django

.. _assets_manager_django:

==========================
Assets manager for Django
==========================

.. seealso::

   - https://www.youtube.com/watch?v=E613X3RBegI

.. toctree::
   :maxdepth: 3

   2019/2019
