
.. index::
   pair: versions ; parcel

.. _parcel_versions:

===================================================================
parcel versions
===================================================================

.. seealso::

   - https://github.com/parcel-bundler/parcel/releases


.. toctree::
   :maxdepth: 3

   1.12.3/1.12.3
