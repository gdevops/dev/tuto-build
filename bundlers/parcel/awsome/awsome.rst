
.. index::
   pair: awsome ; parcel

.. _awsome_parcel:

===================================================================
awsome parcel
===================================================================

.. seealso::

   - https://github.com/parcel-bundler/awesome-parcel
