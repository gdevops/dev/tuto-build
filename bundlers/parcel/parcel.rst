
.. index::
   pair: bundler ; parcel
   ! parcel

.. _parcel:

===================================================================
parcel (Blazing fast, zero configuration web application bundler)
===================================================================

.. seealso::

   - https://parceljs.org/
   - https://github.com/parcel-bundler/parcel
   - https://x.com/parceljs
   - https://x.com/devongovett


.. figure:: logo_parcel.png
   :align: center


.. figure:: ../webpack/webpack_2019_parcel.png
   :align: center

   https://www.youtube.com/watch?v=E613X3RBegI


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   awsome/awsome
