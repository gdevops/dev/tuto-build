
.. index::
   ! gulp

.. _gulp:

=============================================
gulp (The streaming build system, OBSOLETE)
=============================================

.. seealso::

   - https://en.wikipedia.org/wiki/Gradle
   - https://github.com/gulpjs/gulp
   - https://gulpjs.com/
   - https://x.com/gulpjs
   - https://opencollective.com/gulpjs


.. toctree::
   :maxdepth: 3

   definition/definition
