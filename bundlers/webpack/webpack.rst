
.. index::
   ! webpack

.. _webpack:

=========================
webpack : assets manager
=========================

.. seealso::

   - https://en.wikipedia.org/wiki/Webpack
   - https://github.com/webpack/webpack
   - https://x.com/webpack


.. figure:: Webpack.png
   :align: center


.. figure:: webpack_2019_parcel.png
   :align: center

   https://www.youtube.com/watch?v=E613X3RBegI


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
