
.. _webpack_definition:

=====================
webpack definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Webpack

Webpack (stylised webpack) is an open-source JavaScript module bundler.

It is a module bundler primarily for JavaScript, but it can transform front-end
assets like HTML, CSS, even images if the corresponding plugins are included.

Webpack takes modules with dependencies and generates static assets
representing those modules.

Webpack takes the dependencies and generates a dependency graph allowing web
developers to use a modular approach for their web application development
purposes.

It can be used from the command line, or can be configured using a config file
which is named webpack.config.js.
This file is used to define loaders, plugins, etc., for a project.
(Webpack is highly extensible via loaders which allow developers to write
custom tasks that they want to perform when bundling files together.)

A tool named createapp.dev simplifies the process of creating this config file.

Node.js is required for installing Webpack.

Webpack provides code on demand using the moniker code splitting.

The Technical Committee 39 for ECMAScript is working on standardization of a
function that loads additional code: proposal-dynamic-import
