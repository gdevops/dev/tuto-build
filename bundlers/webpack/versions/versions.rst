
.. index::
   pair: webpack; versions

.. _webpack_versions:

==================
webpack versions
==================

.. seealso::

   - https://github.com/webpack/webpack/releases


.. toctree::
   :maxdepth: 3

   5.0.0/5.0.0
   4.31.0/4.31.0
