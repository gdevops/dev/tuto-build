

.. index::
   pair: Best practices ; Build automation


.. _build_practices:

======================
Best practices
======================

.. seealso::

   - https://github.com/coreinfrastructure/best-practices-badge/blob/master/doc/criteria.md#build


.. contents::
   :depth: 3

Working build system
=====================

If the software produced by the project requires building for use, the project
MUST provide a working build system that can automatically rebuild the software
from source code
