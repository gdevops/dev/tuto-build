

.. index::
   ! makefile

.. _makefile:

================
makefile
================

.. seealso::

   - https://www.gnu.org/prep/standards/html_node/Makefile-Basics.html


.. toctree::
   :maxdepth: 3

   shellology/shellology
   tutorials/tutorials
