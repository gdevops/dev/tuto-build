

.. index::
   pair: makefile ; tutorials

.. _makefile_tutorials:

====================
makefile tutorials
====================

.. seealso::

   - https://www.gnu.org/prep/standards/html_node/Makefile-Basics.html


.. toctree::
   :maxdepth: 3

   2010/2010
