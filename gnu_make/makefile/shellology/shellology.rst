

.. index::
   pair: shell ; shellology

.. _shellology:

================
Shellology
================

.. seealso::

   - https://www.gnu.org/prep/standards/html_node/Makefile-Basics.html


.. contents::
   :depth: 3

Introduction
==============

There are several families of shells, most prominently the Bourne family and
the C shell family which are deeply incompatible.

If you want to write portable shell scripts, avoid members of the C shell
family.

The the Shell difference FAQ includes a small history of Posix shells, and a
comparison between several of them.
