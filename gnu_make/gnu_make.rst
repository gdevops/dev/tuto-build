
.. index::
   ! GNU make

.. _gnu_make:

================
GNU make
================

.. seealso::

   - https://fr.wikipedia.org/wiki/GNU_Make
   - https://www.gnu.org/software/make/


.. figure:: GNU_make.png
   :align: center

.. toctree::
   :maxdepth: 3

   makefile/makefile
