
.. index::
   ! CMake

.. _cmake:

================
Cmake
================

.. seealso::

   - https://en.wikipedia.org/wiki/CMake
   - https://gitlab.kitware.com/cmake/cmake
   - https://cmake.org/
   - https://blog.kitware.com/tag/CMake/


.. figure:: Cmake.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
