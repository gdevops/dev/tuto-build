
.. index::
   pair: Cmake ; Definition

.. _cmake_definition:

=====================
Cmake definition
=====================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/CMake


CMake is a cross-platform free and open-source software application for
managing the build process of software using a compiler-independent method.

It supports directory hierarchies and applications that depend on multiple
libraries.
It is used in conjunction with native build environments such as make,
Apple's Xcode, and Microsoft Visual Studio.

It has minimal dependencies, requiring only a C++ compiler on its own build
system.
