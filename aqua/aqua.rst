
.. index::
   ! aqua

.. _aqua:

==================================================================
**aqua** (Declarative CLI Version Manager written in Go)
==================================================================

- https://github.com/aquaproj/aqua
- https://aquaproj.github.io/

Introduction
==============

- https://blog.stephane-robert.info/post/aqua-tools/

A l’image d’asdf, aqua est un nouvel outil permettant d’installer de très
nombreux outils.

Par exemple, vous travaillez pour un client qui utilise Kubernetes 1.27
et Terraform 1.5.0.

Ensuite, on vous met sur un nouveau projet client qui lui utilise Kubernetes 1.25
et Terraform 1.3.6.

Si vous ne gérez pas vos versions correctement cela peut vite tourner au
drame avec des incompatibilités ou du code non pris en charge au moment
de la livraison de celui-ci.


Plus loin
-----------

aqua est assez complet. Il regorge de fonctionnalités comme :

- la gestion des policies. Ces policies permettent de restreindre
  l’installation et l’exécution de packages.
  L’objectif principal de ces politiques est d’améliorer la sécurité en
  empêchant l’exécution d’outils malveillants.
- l’intégration de renovate
- la création d’image docker contenant des outils.

Par contre, tout cela fera l’objet de prochains billets.
