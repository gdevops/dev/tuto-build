
.. index::
   ! Gradle

.. _gradle:

==============================
Gradle
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/Gradle
   - https://github.com/gradle/gradle
   - https://gradle.org/


.. figure:: Gradle_Logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
