

.. _gradle_def:

==============================
Gradle definition
==============================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Gradle

Gradle is an open-source build automation system that builds upon the
concepts of Apache Ant and Apache Maven and introduces a Groovy-based
domain-specific language (DSL) instead of the XML form used by Apache
Maven for declaring the project configuration.

Gradle uses a directed acyclic graph ("DAG") to determine the order in
which tasks can be run.

Gradle was designed for multi-project builds, which can grow to be
quite large.

It supports incremental builds by intelligently determining which parts
of the build tree are up to date; any task dependent only on those parts
does not need to be re-executed.
