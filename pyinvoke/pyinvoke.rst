
.. index::
   ! Pyinvoke
   ! Invoke

.. _pyinvoke:
.. _invoke:

========================
pyinvoke
========================

- https://github.com/pyinvoke/invoke
- http://docs.pyinvoke.org/en/latest/
- https://www.pyinvoke.org/


Installation
=============

::

    pip install invoke


::

    Collecting invoke
      Using cached https://files.pythonhosted.org/packages/be/9f/8508712c9cad73ac0c8eeb2c3e51c9ef65136653dda2b512bde64109f023/invoke-1.2.0-py3-none-any.whl
    Installing collected packages: invoke
    Successfully installed invoke-1.2.0

Description
=============


.. toctree::
   :maxdepth: 3

   definition/definition
   invocations/invocations
   versions/versions
