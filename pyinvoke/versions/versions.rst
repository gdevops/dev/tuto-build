
.. index::
   pair: Invoke ; Versions

.. _invoke_versions:

================
Invoke versions
================

.. seealso::

   - https://github.com/pyinvoke/invoke

.. toctree::
   :maxdepth: 3

   1.2.0/1.2.0
