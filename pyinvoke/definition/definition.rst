
.. index::
   pair: invoke;  definition

.. _invoke_def:

=====================
invoke definition
=====================

.. seealso::

   - https://www.pyinvoke.org/

.. contents::
   :depth: 3

Introduction
=============

This website covers project information for Invoke such as the changelog,
contribution guidelines, development roadmap, news/blog, and so forth.

Detailed usage and API documentation can be found at our code documentation site,
docs.pyinvoke.org.

Please see below for a high level intro, or the navigation on the left for the
rest of the site content.


What is Invoke ?
==================

Invoke is a Python (2.7 and 3.4+) task execution tool & library, drawing
inspiration from various sources to arrive at a powerful & clean feature set.

Like Ruby’s Rake tool and Invoke’s own predecessor Fabric 1.x, it provides a
clean, high level API for running shell commands and defining/organizing task
functions from a tasks.py file:
