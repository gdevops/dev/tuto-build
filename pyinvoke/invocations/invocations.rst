
.. index::
   pair: Invoke ; invocations

.. _invoke_invocations:

====================
Invoke invocations
====================

.. seealso::

   - https://github.com/pyinvoke/invocations
   - https://github.com/pyinvoke/invocations/tree/master/invocations


.. toctree::
   :maxdepth: 3

   docs/docs
