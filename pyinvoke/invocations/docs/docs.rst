
.. index::
   pair: docs ; invocation
   pair: docs ; invocation

.. _docs_invocations:

==========================
Docs (sphinx) invocation
==========================

.. seealso::

   - https://github.com/pyinvoke/invocations/blob/master/invocations/docs.py


.. literalinclude:: docs.py
   :linenos:
